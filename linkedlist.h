#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#include "myexception.h"

template<typename E>
class Iterator;

using namespace std;

template<typename E>
class LinkedList {
public:
    typedef Iterator<E> iterator;

    /**
     * @brief Constructor vacio de la lista
     */
    LinkedList();

    /**
     * @brief Constructor por copia de la lista
     * @param l La lista que se va a copiar
     */
    LinkedList(const LinkedList<E>& l);

    /**
     * @brief Inserta un elemento al comienzo de la lista
     * @param e El elemento que se va a insertar
     */
    void insertFirst(E e);

    /**
     * @brief Inserta un elemento al final de la lista
     * @param e El elemento que se va a insertar
     */
    void insertLast(E e);

    /**
     * @brief Inserta el elemento en la siguiente posicion indicada por el
     * iterador
     * @param e El elemento que se va a insertar
     * @param it El iterador a partir del cual se inserta el elemento
     */
    void insert(E e, typename LinkedList<E>::iterator& it);

    /**
     * @brief remove Elimina la primera aparicion de un elemento en la lista
     * @param e El elemento a eliminar
     * @return TRUE si se ha eliminado, FALSE si el elemento no estaba en la lista
     */
    bool remove(E e);

    /**
     * @brief Elimina todos los elementos contenidos en la lista
     */
    void removeAll();

    /**
     * @brief Concatena, al final de la lista actual, la lista que se pasa como parámetro
     * @param l La lista que se va a concatenar al final
     */
    void concat(const LinkedList<E>& l);

    /**
     * @brief Comprueba si la lista actual esta vacia o no
     * @return TRUE si esta vacia, FALSE en caso contrario
     */
    bool isEmpty() const;

    /**
     * @brief Recupera el primer elemento de la lista (sin eliminarlo)
     * @return El primer elemento de la lista
     */
    E first();

    /**
     * @brief Recupera el ultimo elemento de la lista (sin eliminarlo)
     * @return El ultimo elemento de la lista
     */
    E last();

    /**
     * @brief Obtiene la longitud de la lista
     * @return El numero de elementos almacenados en la lista
     */
    int length() const;

    /**
     * @brief Obtiene el elemento que se encuentra en la posicion index en la lista (sin eliminarlo)
     * @param index Posicion de la que se quiere obtener el elemento
     * @return El elemento almacenado en la posicion index
     */
    E getElement(int index);

    /**
     * @brief Comprueba si el elemento esta en la lista o no
     * @param e El elemento que se quiere comprobar
     * @return TRUE si el elemento esta en la lista, FALSE en caso contrario
     */
    bool contains(E e);

    /**
     * @brief Operacion resto. Devuelve una copia de la lista con todos los elementos salvo el primero
     * @return Una copia de la lista con todos los elementos salvo el primero.
     */
    LinkedList<E> remaining();

    /**
     * @brief Copia la lista que recibe como parametro en la lista actual
     * @param l La lista que se va a copiar
     * @return La lista actual despues de haberla copiado
     */
    LinkedList<E>& operator=(const LinkedList<E>& l);

    /**
     * @brief Comprueba si la lista actual es igual a otra
     * @param l La lista para comparar
     * @return TRUE si las listas tienen los mismos elementos en el mismo orden, FALSE en cualquier otro caso
     */
    bool operator==(const LinkedList<E>& l);

    /**
     * @brief Comprueba si la lista actual es diferente a otra
     * @param l La lista para comparar
     * @return TRUE si las listas no tienen los mismos elementos o no estan en el mismo orden, FALSE en cualquier otro caso
     */
    bool operator!=(const LinkedList<E>& l);

    friend std::ostream& operator<<(std::ostream& os, const LinkedList<E>& l) {
        os << l.length();
        Node* aux=l.head;
        while (aux!=nullptr){
            os << " " << aux->info;
            aux=aux->next;
        }
        return os;
    }

    friend istream& operator>>(istream& is, LinkedList<E>& l) {
        E e;
        int length;
        is >> length;

        for (int i=0;i<length;i++) {
            is >> e;
            l.insertLast(e);
        }
        return is;
    }

    friend class Iterator<E>;

    iterator begin();
    iterator end();

private:
    struct Node
    {
        E info;
        Node* next = nullptr;
    };
    Node* head;
};


template <typename E>
LinkedList<E>::LinkedList()
{
    head = nullptr;
}

template <typename E>
LinkedList<E>::LinkedList(const LinkedList<E>& l)
{
    Node * pAuxL = l.head;
    head = nullptr;
    head->info = pAuxL->info;
    Node * auxThis = head;
    pAuxL = pAuxL->next;
    while (pAuxL != nullptr)
    {
        auxThis->next = new Node;
        auxThis = auxThis->next;
        auxThis->info = pAuxL->info;
        pAuxL = pAuxL->next;
    }
}

template <typename E>
int LinkedList<E>::length() const
{
    Node * aux = head;
    int size = 0;
    while (aux != nullptr)
    {
        size++;
        aux = aux->next;
    }
    return size;
}

template <typename E>
void LinkedList<E>::insertFirst(E e)
{
    Node * aux = new Node;
    aux->info = e;
    aux->next = head;
    head = aux;
}

template <typename E>
E LinkedList<E>::getElement(int index)
{
    Node * aux = head;
    int cont = 0;
    while ((aux != nullptr) && (cont < index))
    {
        aux = aux->next;
        cont++;
    }
    if (cont < index)
    {
        throw MyException("Index is larger than size");
    }
    return aux->info;
}

template <typename E>
bool LinkedList<E>::remove(E e)     // need refactor
{
    Node * pActual = head;
    Node * pAnterior;
    bool borrado = false;

    // Lista contiene 1 elemento y el primer elemento es 'e'
    if ((pActual != nullptr) && (pActual->info == e)
            && (pActual->next == nullptr))
    {
        delete pActual;
        head = nullptr;
        borrado = true;
    }
    // Lista contiene >1 elemento y el primer elemento es 'e'
    else if ((pActual != nullptr) && (pActual->info == e)
             && (pActual->next != nullptr))
    {
        head = head->next;
        delete pActual;
        borrado = true;
    }
    // Lista contiene >1 elemento
    while ((pActual != nullptr) && !borrado)
    {
        if (pActual->info == e)
        {
            pAnterior->next = pActual->next;
            delete pActual;
            borrado = true;
        }
        else
        {
            pAnterior = pActual;
            pActual = pActual->next;
        }
    }
    return borrado;
}

template <typename E>
void LinkedList<E>::insertLast(E e)
{
    Node * pAnterior;
    Node * pActual = head;
    if (this->isEmpty())
    {
        head = new Node;
        head->info = e;
    }
    else
    {
        while (pActual != nullptr)
        {
            pAnterior = pActual;
            pActual = pActual->next;
        }
        pAnterior->next = new Node();
        pAnterior = pAnterior->next;
        pAnterior->info = e;
    }
}

template <typename E>
void LinkedList<E>::removeAll()
{
    Node* pAnterior;
    Node* pActual = head;
    while (pActual != nullptr)
    {
        pAnterior = pActual;
        pActual = pActual->next;
        delete pAnterior;
    }
    head = nullptr;
}

template <typename E>
void LinkedList<E>::concat(const LinkedList<E>& l)
{
    Node* auxThis = head;
    Node* auxL = l.head;

    // Mover puntero a la ultima posicion de this
    if (auxThis != nullptr)
    {
        while (auxThis->next != nullptr)
        {
            auxThis = auxThis->next;
        }
    }
    // this esta vacio, crear head y copiar el 1º elemento de l donde apunta head
    else if (auxL != nullptr)
    {
        head = new Node;
        auxThis = head;
        auxThis->info = auxL->info;
        auxL = auxL->next;
    }
    while (auxL != nullptr)
    {
        auxThis->next = new Node;
        auxThis = auxThis->next;
        auxThis->info = auxL->info;
        auxL = auxL->next;
    }
}

template <typename E>
bool LinkedList<E>::isEmpty() const
{
    return (head == nullptr);
}

template <typename E>
E LinkedList<E>::first()
{
    if (isEmpty())
    {
        throw MyException("The list is empty");
    }
    return (head->info);
}

template <typename E>
E LinkedList<E>::last()
{
    if (isEmpty())
    {
        throw MyException("The list is empty");
    }
    Node * pAnterior;
    Node * pActual = head;
    while (pActual != nullptr)
    {
        pAnterior = pActual;
        pActual = pActual->next;
    }
    return pAnterior->info;
}

template <typename E>
bool LinkedList<E>::contains(E e)
{
    Node* pActual = head;
    bool find = false;
    while ((pActual != nullptr) && !find )
    {
        if (pActual->info == e)
        {
            find = true;
        }
        else
        {
            pActual = pActual->next;
        }
    }
    return find;
}

template <typename E>
LinkedList<E> LinkedList<E>::remaining()
{
    LinkedList<E> remain;
    remain.head = new Node;
    Node* auxThis = head->next;
    Node* aux2 = remain.head;

    while (auxThis != nullptr)
    {
        aux2->info = auxThis->info;
        auxThis = auxThis->next;
        if (auxThis != nullptr)
        {
            aux2->next = new Node;
            aux2 = aux2->next;
        }
    }
    return remain;
}

template <typename E>
LinkedList<E>& LinkedList<E>::operator = (const LinkedList<E>& l)
{
    Node* pointThis = head;
    Node* pointL = l.head;
    Node* pointIfThis;      // puntero anterior a pointThis

    while ((pointThis != nullptr) && (pointL != nullptr))
    {
        pointThis->info = pointL->info;
        pointIfThis = pointThis;
        pointThis= pointThis->next;
        pointL = pointL->next;
    }

    // this no le queda memoria para seguir copiando elementos de l
    // this pedir memoria dinamica
    if ((pointThis == nullptr) && (pointL != nullptr))
    {
        while (pointL != nullptr)
        {
            if (this->isEmpty())
            {
                head = new Node;
                pointIfThis = head;
                pointIfThis->info = pointL->info;
            }
            else
            {
                pointIfThis->next = new Node;
                pointIfThis = pointIfThis->next;
                pointIfThis->info = pointL->info;
            }
            pointL = pointL->next;
        }
    }
    // this todavia le queda nodos sobrantes
    // liberar memoria dinamica sobrante
    else if ((pointL == nullptr) && (pointThis != nullptr))
    {
        Node* pDelete = pointIfThis;
        pointIfThis = pointIfThis->next;
        pDelete->next = nullptr;       // cerrar la lista
        while (pointIfThis != nullptr)
        {
            pDelete = pointIfThis;
            pointIfThis = pointIfThis->next;
            delete pDelete;
        }
    }
    return *this;
}

template <typename E>
bool LinkedList<E>::operator==(const LinkedList<E>& l)
{
    Node* aux1 = head;
    Node* aux2 = l.head;
    bool equals = true;

    while ((aux1 != nullptr) && (aux2 != nullptr) && equals)
    {
        // UNA de las dos listas ya no contiene mas elementos
        if (((aux1 == nullptr) && (aux2 != nullptr)) ||
                ((aux1 != nullptr) && (aux2 == nullptr)))
        {
            equals = false;
        }
        else if (aux1->info != aux2->info)
        {
            equals = false;
        }
        else
        {
            aux1 = aux1->next;
            aux2 = aux2->next;
        }
    }
    return equals;
}

template <typename E>
bool LinkedList<E>::operator!=(const LinkedList<E>& l)
{
    return !(*this == l);
}

#endif // LINKEDLIST_H
